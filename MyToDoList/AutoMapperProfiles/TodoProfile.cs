﻿using AutoMapper;
using MyToDoList.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TDLData.Models;

namespace MyToDoList.AutoMapperProfiles
{
    public class TodoProfile: Profile
    {
        public TodoProfile()
        {
            CreateMap<MyList, TDLViewModel>();
        }
    }
}
