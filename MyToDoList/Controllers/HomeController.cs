﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MyToDoList.ViewModels;
using TDLData;
using TDLData.Models;


namespace MyToDoList.Controllers
{
    public class HomeController : Controller
    {
        private IToDoList _myList;
        private readonly IMapper _mapper;

        public HomeController(IToDoList myList)
        {
            _myList = myList;
        }

        public HomeController(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var TDLmodels = _myList.GetAll();
            IEnumerable<TDLViewModel> viewModel = _mapper.Map<IEnumerable<MyList>,List<TDLViewModel>>(TDLmodels);
            return View("~/Views/Home/Index.cshtml", viewModel);
        }
    }
}