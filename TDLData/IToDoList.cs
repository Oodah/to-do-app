﻿using System;
using System.Collections.Generic;
using System.Text;
using TDLData.Models;

namespace TDLData
{
    public interface IToDoList
    {
        void Add(MyList myList);
        MyList getTDLbyID(int id);

        IEnumerable<MyList> GetAll();
    }
}
