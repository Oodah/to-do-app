﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDLData.Models
{
   public class MyList
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }

    }
}
