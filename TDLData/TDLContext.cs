﻿using Microsoft.EntityFrameworkCore;
using System;
using TDLData.Models;

namespace TDLData
{
    public class TDLContext : DbContext
    {
        public TDLContext(DbContextOptions options) : base(options) { }
        public DbSet<MyList> MyList { get; set; }
    }
}
