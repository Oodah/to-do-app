﻿using System;
using System.Collections.Generic;
using TDLData;
using TDLData.Models;

namespace TDLServices
{

    public class ToDoListServices : IToDoList
    {
        private TDLContext _context;

        public ToDoListServices(TDLContext context)
        {
            _context = context;
        }
        public void Add(MyList myList)
        {
            _context.Add(myList);
            _context.SaveChanges();

        }

        public MyList getTDLbyID(int id)
        {
            return _context.MyList.Find(id);
        }

        public IEnumerable<MyList> GetAll()
        {
            return _context.MyList;
        }
    }
}
